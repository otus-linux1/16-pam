#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#include <security/pam_appl.h>
#include <security/pam_modules.h>

/*
 * helpers
 *
 * https://man7.org/linux/man-pages/man3/getgrouplist.3.html
 * https://www.freebsd.org/doc/ru/articles/pam/pam-sample-module.html
 * https://stackoverflow.com/questions/22104383/proper-way-to-get-groups-of-a-user-in-linux-using-c
*/

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv) {

    int retval;  // pam return value
    const char* weekend_group = "admin";  // special group
    const char* user;  // current user
    struct passwd *pw;
    int ngroups = 0; // user groups count

    if (!is_weekend()) {
        return PAM_SUCCESS;
    }

    // get username
	retval = pam_get_user(pamh, &user, NULL);
	if (retval != PAM_SUCCESS) {
        printf("get user failed %d\n", retval);
	    return retval;
	}
    printf("Welcome %s\n", user);

	// fetch passwd structure
    pw = getpwnam(user);
    if (pw == NULL) {
        printf("could not fetch passwd info for user");
        return PAM_USER_UNKNOWN;
    }

    //this call is just to get the correct ngroups
    getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);

    gid_t groups[ngroups];
    // actually get groups
    getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

    for (int i = 0; i < ngroups; i++) {
        struct group *gr = getgrgid(groups[i]);
        if (gr == NULL) {
            continue;
        }
        // success if user group matches special group
        if (strcmp(gr->gr_name, weekend_group) == 0) {
            return PAM_SUCCESS;
        }
    }

    return PAM_AUTH_ERR;
}

bool is_weekend() {
    time_t curr_timestamp = time(NULL);
    struct tm* dt = localtime(&curr_timestamp);
    return dt->tm_wday == 0 || dt->tm_wday == 6;
}

/* stub for expected hook */
PAM_EXTERN int pam_sm_setcred( pam_handle_t *pamh, int flags, int argc, const char **argv ) {
	return PAM_SUCCESS;
}

/* stub for expected hook */
PAM_EXTERN int pam_sm_authenticate( pam_handle_t *pamh, int flags,int argc, const char **argv ) {
	return PAM_SUCCESS;
}
