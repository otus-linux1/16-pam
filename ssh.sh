#! /usr/bin/env bash

if [[ -z $1 ]]; then
    echo "user name is required"
    exit 1
fi

vagrant_ssh_port=$(vagrant ssh-config| grep Port | awk '{print $2}')
ssh -o StrictHostKeyChecking=no \
    -o UserKnownHostsFile=/dev/null \
    -p $vagrant_ssh_port \
    ${1}@127.0.0.1
